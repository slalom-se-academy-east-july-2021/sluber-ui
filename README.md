# README

The SLUBER UI is a React UI.

## Installation

* Clone repo: [https://bitbucket.org/slalom-se-academy-east-july-2021/sluber-ui](https://bitbucket.org/slalom-se-academy-east-july-2021/sluber-ui)

## Building and running the app
* Build:  From the command line, navigate to the repo folder and run `npm install`
* Run: From the command line, navigate to the repo folder and run `npm run start`

## Building and running the docker image
* Build Docker image: From the command line, run `docker build -t sluber-ui -f ./Dockerfile` .
* Run Docker image:  From the command line, run `docker run -p 3000:3000  --name sluber-ui sluber-ui`

import { makeStyles } from '@material-ui/core/styles';

const CardListStyles = makeStyles({
        grid: {
            "align-items": "center",
        },
        card: {
            "margin-bottom": "1%",
            "border": "2px solid black",
        }
    });

export default CardListStyles;